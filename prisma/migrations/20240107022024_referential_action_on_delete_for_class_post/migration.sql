-- DropForeignKey
ALTER TABLE "PostComment" DROP CONSTRAINT "PostComment_class_post_id_fkey";

-- AddForeignKey
ALTER TABLE "PostComment" ADD CONSTRAINT "PostComment_class_post_id_fkey" FOREIGN KEY ("class_post_id") REFERENCES "ClassPost"("id") ON DELETE CASCADE ON UPDATE CASCADE;
