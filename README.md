# Environment setup

## 1. Install all node modules.

Open the terminal in the source code folder and run the following command to install all the necessary node modules:

“npm i”

## 2. Run this service with only docker compose(optional).

We have prepared a Dockerfile and Docker Compose file for the project. To run this service. If you want to create a
database then uncommented the db service in docker compose, applied the same thing to creating containers for PostgreSQL and Redis.
Then run:

“docker compose up -d --build”

If you prefer to test the run the source code manually. then skip this step.

## 3. Create .env file

The source code requires a .env file to store secret data. Create a .env file in the source code folder with the following content:

DATABASE_URL=postgres://myuser:mypassword@localhost:5433/my-db
MAIL_QUEUE_URL=redis://default:eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81@localhost:6379
ROLE_CREATOR=creator
ROLE_TEACHER=teacher
ROLE_STUDENT=student
INVITATION_SECRET_KEY=RTG
MAIL_TOKEN_EXPIRED_TIME=604800
INVITATION_SECRET_KEY=RTG
FE_INVITATION_CALL_BACK_URL=http://localhost:5173/invitation/by-mail #this is our local frontend redirect url address.
MAIL_TOKEN_EXPIRED_TIME=604800

Note 1: These values are based on the database and Redis created via Docker Compose in section 2. If testing on a deployed database, use the provided alternative values. You can also replace them with your desired values.
Note 2: Please enter your own social app id and secret to use social login route,
Note 3: Please use your Front End redirect url
Note 4: You can use our deployed redis instead "redis://default:O9Dr2y87Sv2cYf61WOVZCvvNTWqdq2Gb@redis-11353.c295.ap-southeast-1-1.ec2.redns.redis-cloud.com:11353"

## 4. Prisma

### Prisma migrate DB

If you want to automatically generate the database, run the following command and confirm with "y":

“npx prisma migrate dev”

### Alternatively, if you want to connect to an existing database, run:

“npx prisma introspect” or "npx prisma db pull"

## 5. Start the project

Run “npx nest start” to start the project.

## 6. Swagger.

We have created a quick document for testing the API. Access it by redirecting to the "/api" route.

The script will automatically generate User table and some dummy data for you!
