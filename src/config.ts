require('dotenv').config();
class AppConfig {
  DATABASE_URL: string = process.env.DATABASE_URL;
  MAIL_QUEUE_URL: string = process.env.MAIL_QUEUE_URL;
  ROLE_CREATOR: string = process.env.ROLE_CREATOR;
  ROLE_TEACHER: string = process.env.ROLE_TEACHER;
  ROLE_STUDENT: string = process.env.ROLE_STUDENT;
  INVITATION_SECRET_KEY = process.env.INVITATION_SECRET_KEY;
  FE_INVITATION_CALL_BACK_URL = process.env.FE_INVITATION_CALL_BACK_URL;
  MAIL_TOKEN_EXPIRED_TIME: number = parseInt(
    process.env.MAIL_TOKEN_EXPIRED_TIME,
  );
  NOTIFICATION_QUEUE_URL = process.env.NOTIFICATION_QUEUE_URL;
}

export const appConfig = new AppConfig();
