import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { CreateClassroomDto } from './dto/create-classroom.dto';
import { UpdateClassroomDto } from './dto/update-classroom.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { appConfig } from 'src/config';
import { EnrollmentService } from 'src/enrollment/enrollment.service';
import { InviteBymailDto } from './dto/invite-by-mail.dto';
import { JwtService } from '@nestjs/jwt';
import { AddTeacherByToken } from './dto/add-teacher-by-token.dto';
import { MailProducerService } from 'src/mail-producer/mail-producer.service';
import { CreateClassPost, UpdateClassPost } from './dto/class-post.dto';
import { NotificationProducerService } from 'src/notification-producer/notification-producer.service';

@Injectable()
export class ClassroomService {
  constructor(
    private prismaService: PrismaService,
    @Inject(forwardRef(() => EnrollmentService))
    private enrollmentService: EnrollmentService,
    private jwtService: JwtService,
    private mailProducerService: MailProducerService,
    private notisProducerService: NotificationProducerService,
  ) {}
  async create(createClassroomDto: CreateClassroomDto, id: number) {
    //check if id exist

    const newClass = await this.prismaService.class.create({
      data: {
        name: createClassroomDto.name,
        section: createClassroomDto.section,
        subject: createClassroomDto.subject,
        room: createClassroomDto.room,
        users: {
          create: [
            {
              role: appConfig.ROLE_CREATOR,
              user_entity: {
                connect: {
                  id: id,
                },
              },
            },
          ],
        },
      },
    });

    return newClass;
  }

  async findOne(classID: string, userID: number) {
    const isInClass = await this.enrollmentService.isUserInClass(
      userID,
      classID,
    );
    if (!isInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const foundClass = await this.prismaService.class.findUnique({
      where: {
        id: classID,
      },
      include: {
        users: {
          where: {
            role: appConfig.ROLE_CREATOR,
          },
        },
      },
    });
    return foundClass;
  }

  async isExistedClass(id: string) {
    const foundClass = await this.prismaService.class.findUnique({
      where: {
        id: id,
      },
    });
    if (foundClass !== null) {
      return true;
    }
    return false;
  }

  async classWithInviteCode(code: string) {
    const foundClass = await this.prismaService.class.findUnique({
      where: {
        invite_code: code,
      },
    });
    return foundClass;
  }

  async updateInfo(updateClassroomDto: UpdateClassroomDto, userID: number) {
    const isManagerInClass = await this.enrollmentService.isManagerInClass(
      userID,
      updateClassroomDto.class_id,
    );
    if (!isManagerInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    //update
    const { class_id, ...data } = updateClassroomDto;
    const updatedClass = await this.prismaService.class.update({
      where: {
        id: updateClassroomDto.class_id,
      },
      data: data,
    });
    return updatedClass;
  }

  async inviteStudentsByMail(
    inviteStudentBymailDto: InviteBymailDto,
    userID: number,
  ) {
    const isManagerInClass = await this.enrollmentService.isManagerInClass(
      userID,
      inviteStudentBymailDto.class_id,
    );
    if (!isManagerInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    inviteStudentBymailDto.mail.forEach((email) => {
      const payload = {
        email: email,
        class_id: inviteStudentBymailDto.class_id,
        role: appConfig.ROLE_STUDENT,
      };
      const token = this.jwtService.sign(payload, {
        expiresIn: appConfig.MAIL_TOKEN_EXPIRED_TIME,
        algorithm: 'HS256',
        secret: appConfig.INVITATION_SECRET_KEY,
      });
      const _payload = {
        role: appConfig.ROLE_STUDENT,
        email: email,
        token: token,
        url:
          appConfig.FE_INVITATION_CALL_BACK_URL +
          '?token=' +
          token +
          '&role=' +
          appConfig.ROLE_STUDENT,
      };
      this.mailProducerService.inviteStudent(_payload);
    });
    return 'success';
  }

  async inviteTeachersByMail(
    inviteTeacherBymailDto: InviteBymailDto,
    userID: number,
  ) {
    const isManagerInClass = await this.enrollmentService.isManagerInClass(
      userID,
      inviteTeacherBymailDto.class_id,
    );
    if (!isManagerInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    inviteTeacherBymailDto.mail.forEach((email) => {
      const payload = {
        email: email,
        class_id: inviteTeacherBymailDto.class_id,
        role: appConfig.ROLE_TEACHER,
      };
      const token = this.jwtService.sign(payload, {
        expiresIn: appConfig.MAIL_TOKEN_EXPIRED_TIME,
        algorithm: 'HS256',
        secret: appConfig.INVITATION_SECRET_KEY,
      });
      const _payload = {
        role: appConfig.ROLE_TEACHER,
        token: token,
        email: email,
        url:
          appConfig.FE_INVITATION_CALL_BACK_URL +
          '?token=' +
          token +
          '&role=' +
          appConfig.ROLE_TEACHER,
      };
      this.mailProducerService.inviteStudent(_payload);
    });
    return 'success';
  }

  async addByToken(
    addTeacherByToken: AddTeacherByToken,
    userID: number,
    role: string,
  ) {
    let result: {
      email: string;
      class_id: string;
      role: string;
      iat: number;
      exp: number;
    };
    try {
      result = this.jwtService.verify(addTeacherByToken.token || ' ', {
        secret: appConfig.INVITATION_SECRET_KEY,
      });
    } catch (error) {
      // console.log(error);
      throw new HttpException(
        'Token provided is invalid',
        HttpStatus.FORBIDDEN,
      );
    }

    if (role !== result.role) {
      throw new HttpException(
        `You are not invited to class as a ${role}`,
        HttpStatus.FORBIDDEN,
      );
    }
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userID,
      },
    });
    if (user.email !== result.email) {
      throw new HttpException(
        'Token provided is invalid',
        HttpStatus.FORBIDDEN,
      );
    }
    const isExistedClass = await this.isExistedClass(result.class_id || '');
    if (!isExistedClass) {
      throw new HttpException(
        'Token provided is invalid',
        HttpStatus.FORBIDDEN,
      );
    }

    const isInClass = await this.enrollmentService.isUserInClass(
      user.id,
      result.class_id || '',
    );
    if (isInClass) {
      throw new HttpException(
        'User already exists in class',
        HttpStatus.CONFLICT,
      );
    }

    const newEnrollment = await this.prismaService.enrollment.create({
      data: {
        class_id: result.class_id,
        user_id: user.id,
        role: role,
      },
      include: {
        class_entity: true,
      },
    });
    return newEnrollment;
  }

  async addTeacherByToken(
    addTeacherByToken: AddTeacherByToken,
    userID: number,
  ) {
    return await this.addByToken(
      addTeacherByToken,
      userID,
      appConfig.ROLE_TEACHER,
    );
  }

  async addStudentByToken(
    addTeacherByToken: AddTeacherByToken,
    userID: number,
  ) {
    return await this.addByToken(
      addTeacherByToken,
      userID,
      appConfig.ROLE_STUDENT,
    );
  }

  async toogleEnableInviteCode(userID: number, classId: string) {
    const isManagerInClass = await this.enrollmentService.isManagerInClass(
      userID,
      classId,
    );
    if (!isManagerInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const foundClass = await this.prismaService.class.findUnique({
      where: {
        id: classId,
      },
    });

    let inviteCode = null;

    if (foundClass.invite_code === null) {
      //enable invite code
      while (inviteCode === null) {
        const generatedCode = this.generateInviteCode(classId, 7);
        const classWithGeneratedCode =
          await this.prismaService.class.findUnique({
            where: {
              invite_code: generatedCode,
            },
          });
        if (classWithGeneratedCode === null) {
          inviteCode = generatedCode;
        }
      }
    }

    await this.prismaService.class.update({
      where: {
        id: classId,
      },
      data: {
        invite_code: inviteCode,
      },
    });

    return { inviteCode };
  }

  generateInviteCode(classId: string, length: number): string {
    // let result = '';
    // let counter = 0;
    // while (counter < length) {
    //   result += classId.charAt(Math.floor(Math.random() * classId.length));
    //   counter += 1;
    // }
    // return result;
    return (Math.random() + 1)
      .toString(36)
      .substring(length - 2)
      .toLocaleLowerCase();
  }

  async createClassPost(userId: number, classPost: CreateClassPost) {
    const isInClass = await this.enrollmentService.isUserInClass(
      userId,
      classPost.class_id,
    );
    if (!isInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const newClassPost = await this.prismaService.classPost.create({
      data: {
        class_id: classPost.class_id,
        content: classPost.content,
        author_id: userId,
      },
      include: {
        author: true,
      },
    });

    const author = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });

    const curClass = await this.prismaService.class.findUnique({
      where: {
        id: classPost.class_id,
      },
    });

    let title =
      classPost.title ||
      `${author.last_name} ${author.first_name} posted a new post in class ${curClass.name}`;
    let link = `/class/${classPost.class_id}/post/${newClassPost.id}`;

    this.notisProducerService.createClassPost({
      classPost: newClassPost,
      type: 'class-post',
      title: title,
      link: link,
    });

    return newClassPost;
  }

  async updateClassPost(
    userId: number,
    updateClassPost: UpdateClassPost,
    postId: number,
  ) {
    const classPost = await this.prismaService.classPost.findUnique({
      where: {
        id: postId,
      },
    });

    if (classPost === null) {
      throw new HttpException("Post doesn't exist", HttpStatus.BAD_REQUEST);
    }

    if (userId !== classPost.author_id) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const updatedClassPost = await this.prismaService.classPost.update({
      where: {
        id: postId,
      },
      data: {
        content: updateClassPost.content,
        updated_at: new Date(),
      },
      include: {
        author: true,
        comments: {
          orderBy: {
            created_at: 'asc',
          },
          include: {
            user: true,
          },
        },
      },
    });

    return updatedClassPost;
  }

  async deleteClassPost(userId: number, postId: number) {
    const classPost = await this.prismaService.classPost.findUnique({
      where: {
        id: postId,
      },
    });

    const isManagerInClass = await this.enrollmentService.isManagerInClass(
      userId,
      classPost.class_id,
    );

    if (classPost === null) {
      throw new HttpException("Post doesn't exist", HttpStatus.BAD_REQUEST);
    }

    if (userId !== classPost.author_id && isManagerInClass === false) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const deletedPost = await this.prismaService.classPost.delete({
      where: {
        id: postId,
      },
    });

    return deletedPost;
  }

  async getClassPost(userId: number, postId: number) {
    const classPost = await this.prismaService.classPost.findUnique({
      where: {
        id: postId,
      },
      include: {
        author: true,
        comments: {
          orderBy: {
            created_at: 'asc',
          },
          include: {
            user: true,
          },
        },
      },
    });

    if (classPost === null) {
      throw new HttpException("Post doesn't exist", HttpStatus.BAD_REQUEST);
    }

    const isInClass = await this.enrollmentService.isUserInClass(
      userId,
      classPost.class_id,
    );

    if (isInClass === false) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    return classPost;
  }

  async getClassPostWithPagination(
    userId: number,
    classId: string,
    lastPostId: number,
  ) {
    const isInClass = await this.enrollmentService.isUserInClass(
      userId,
      classId,
    );
    if (isInClass === false) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const pageSize = 10; // Adjust the page size as needed
    if (lastPostId !== -1) {
      const classPost = await this.prismaService.classPost.findMany({
        where: {
          class_id: classId,
          id: {
            lt: lastPostId,
          },
        },
        include: {
          author: true,
          comments: {
            orderBy: {
              created_at: 'asc',
            },
            include: {
              user: true,
            },
          },
        },
        orderBy: {
          id: 'desc',
        },
        take: pageSize,
      });
      return classPost;
    } else {
      const classPost = await this.prismaService.classPost.findMany({
        where: {
          class_id: classId,
        },
        include: {
          author: true,
          comments: {
            orderBy: {
              created_at: 'asc',
            },
            include: {
              user: true,
            },
          },
        },
        orderBy: {
          id: 'desc',
        },
        take: pageSize,
      });
      return classPost;
    }
  }

  async createPostComment(userId: number, postId: number, content: string) {
    const classPost = await this.prismaService.classPost.findUnique({
      where: {
        id: postId,
      },
      include: {
        class: true,
      },
    });

    if (classPost === null) {
      throw new HttpException("Post doesn't exist", HttpStatus.BAD_REQUEST);
    }

    const isInClass = await this.enrollmentService.isUserInClass(
      userId,
      classPost.class_id,
    );

    if (isInClass === false) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const comment = await this.prismaService.postComment.create({
      data: {
        class_post_id: classPost.id,
        content: content,
        user_id: userId,
      },
      include: {
        user: true,
      },
    });

    if (userId === classPost.author_id) {
      const users = await this.prismaService.postComment.findMany({
        where: {
          class_post_id: classPost.id,
          user_id: {
            not: classPost.author_id,
          },
        },
        distinct: ['user_id'],
      });

      const receiverIds = await Promise.all(
        users.map(async (u) => {
          if (
            await this.enrollmentService.isUserInClass(
              u.user_id,
              classPost.class_id,
            )
          ) {
            return u.user_id;
          }
          return null; // Return null for users not in class
        }),
      );

      const filteredReceiverIds = receiverIds.filter((id) => id !== null);
      const uniqueIds = [...new Set(filteredReceiverIds.map((item) => item))];
      const title = `Someone commented on the post you commented in class ${classPost.class.name}`;
      this.notisProducerService.createPostComment({
        receiverIds: uniqueIds,
        title: title,
        link: `/class/${classPost.class_id}/post/${postId}`,
        comment: comment,
        type: 'class-post',
      });
    } else {
      const users = await this.prismaService.postComment.findMany({
        where: {
          class_post_id: classPost.id,
          user_id: {
            not: userId,
          },
        },
        distinct: ['user_id'],
      });
      const receiverIds = await Promise.all(
        users.map(async (u) => {
          if (
            await this.enrollmentService.isUserInClass(
              u.user_id,
              classPost.class_id,
            )
          ) {
            return u.user_id;
          }
          return null; // Return null for users not in class
        }),
      );

      const filteredReceiverIds = receiverIds.filter(
        (id) => id !== null && id !== classPost.author_id,
      );
      const uniqueIds = [...new Set(filteredReceiverIds.map((item) => item))];
      if (
        await this.enrollmentService.isUserInClass(
          classPost.author_id,
          classPost.class_id,
        )
      ) {
        const titleAuthor = `Someone commented on your post in class ${classPost.class.name}`;
        this.notisProducerService.createPostComment({
          receiverIds: [classPost.author_id],
          title: titleAuthor,
          link: `/class/${classPost.class_id}/post/${postId}`,
          comment: comment,
          type: 'class-post',
        });
      }

      const title = `Someone commented on the post you commented in class ${classPost.class.name}`;
      this.notisProducerService.createPostComment({
        receiverIds: uniqueIds,
        title: title,
        link: `/class/${classPost.class_id}/post/${postId}`,
        comment: comment,
        type: 'class-post',
      });
    }

    return comment;
  }
}
