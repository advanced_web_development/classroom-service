import { Module, forwardRef } from '@nestjs/common';
import { ClassroomService } from './classroom.service';
import { ClassroomController } from './classroom.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { EnrollmentModule } from 'src/enrollment/enrollment.module';
import { JwtModule } from '@nestjs/jwt';
import { MailProducerModule } from 'src/mail-producer/mail-producer.module';
import { NotificationProducerModule } from 'src/notification-producer/notification-producer.module';

@Module({
  exports: [ClassroomService],
  controllers: [ClassroomController],
  providers: [ClassroomService],
  imports: [
    PrismaModule,
    forwardRef(() => EnrollmentModule),
    JwtModule,
    MailProducerModule,
    NotificationProducerModule,
  ],
})
export class ClassroomModule {}
