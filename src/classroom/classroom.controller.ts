import {
  Controller,
  Get,
  Post,
  Put,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  HttpException,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { ClassroomService } from './classroom.service';
import { CreateClassroomDto } from './dto/create-classroom.dto';
import { UpdateClassroomDto } from './dto/update-classroom.dto';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { InviteBymailDto } from './dto/invite-by-mail.dto';
import { AddTeacherByToken } from './dto/add-teacher-by-token.dto';
import {
  CreateClassPost,
  CreatePostComment,
  UpdateClassPost,
} from './dto/class-post.dto';

@ApiTags('classroom')
@ApiHeader({ name: 'id', description: 'id user' })
@Controller('classroom')
export class ClassroomController {
  constructor(private readonly classroomService: ClassroomService) {}

  @Post()
  async create(
    @Body() createClassroomDto: CreateClassroomDto,
    @Req() request: Request,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.create(
      createClassroomDto,
      +request.headers.id,
    );
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @Req() request: Request) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.findOne(id, +request.headers.id);
  }

  @Put()
  async Update(
    @Req() request: Request,
    @Body() updateClassroomDto: UpdateClassroomDto,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.updateInfo(
      updateClassroomDto,
      +request.headers.id,
    );
  }

  @Post('/invite/students/by-email')
  async inviteStudentsByMail(
    @Body() inviteStudentBymailDto: InviteBymailDto,
    @Req() request: Request,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.inviteStudentsByMail(
      inviteStudentBymailDto,
      +request.headers.id,
    );
  }

  @Post('/invite/teachers/by-email')
  async inviteTeachersByMail(
    @Body() inviteTeacherBymailDto: InviteBymailDto,
    @Req() request: Request,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.inviteTeachersByMail(
      inviteTeacherBymailDto,
      +request.headers.id,
    );
  }

  @Post('/add-teacher-by-token')
  async addTeacherByToken(
    @Body() addTeacherByToken: AddTeacherByToken,
    @Req() request: Request,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.addTeacherByToken(
      addTeacherByToken,
      +request.headers.id,
    );
  }

  @Post('/add-student-by-token')
  async addStudentByToken(
    @Body() addStudentByToken: AddTeacherByToken,
    @Req() request: Request,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.addStudentByToken(
      addStudentByToken,
      +request.headers.id,
    );
  }

  @Post('/toggle-enable-invite-code/:id')
  async toogleEnableInviteCode(
    @Req() request: Request,
    @Param('id') classId: string,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.toogleEnableInviteCode(
      +request.headers.id,
      classId,
    );
  }

  @Post('/class-post')
  async createClassPost(
    @Req() request: Request,
    @Body() classPost: CreateClassPost,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.createClassPost(
      +request.headers.id,
      classPost,
    );
  }

  @Put('/class-post/:id')
  async updateClassPost(
    @Req() request: Request,
    @Param('id') postId: number,
    @Body() classPost: UpdateClassPost,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    const id = +postId;
    if (isNaN(id)) {
      throw new HttpException('Invalid post id', HttpStatus.BAD_REQUEST);
    }

    return await this.classroomService.updateClassPost(
      +request.headers.id,
      classPost,
      id,
    );
  }

  @Delete('/class-post/:id')
  async deleteClassPost(@Req() request: Request, @Param('id') postId: number) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    const id = +postId;
    if (isNaN(id)) {
      throw new HttpException('Invalid post id', HttpStatus.BAD_REQUEST);
    }

    return await this.classroomService.deleteClassPost(+request.headers.id, id);
  }

  @Get('/class-post/:id')
  async getClassPost(@Req() request: Request, @Param('id') postId: number) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    const id = +postId;
    if (isNaN(id)) {
      throw new HttpException('Invalid post id', HttpStatus.BAD_REQUEST);
    }

    return await this.classroomService.getClassPost(+request.headers.id, id);
  }

  @Get('/all-class-posts/:classId')
  async getClassPostWithPagination(
    @Req() request: Request,
    @Param('classId') classId: string,
    @Query('lastPostId') lastPostId: number,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    const lastId = +lastPostId;
    if (isNaN(lastId)) {
      throw new HttpException('Invalid post id', HttpStatus.BAD_REQUEST);
    }

    return await this.classroomService.getClassPostWithPagination(
      +request.headers.id,
      classId,
      lastId,
    );
  }

  @Post('/post-comment')
  async createPostComment(
    @Req() request: Request,
    @Body() comment: CreatePostComment,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.classroomService.createPostComment(
      +request.headers.id,
      +comment.postId,
      comment.content,
    );
  }
}
