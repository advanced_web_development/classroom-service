import { ApiProperty } from '@nestjs/swagger';

export class InviteBymailDto {
  @ApiProperty({
    default: '["huynhluat.dn123@gmail.com", "huynhluat.dn1234@gmail.com"]',
  })
  mail: string[];

  @ApiProperty({
    default: 'abc',
  })
  class_id: string;
}
