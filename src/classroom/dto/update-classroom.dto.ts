import { PartialType } from '@nestjs/swagger';
import { CreateClassroomDto } from './create-classroom.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class UpdateClassroomDto extends PartialType(CreateClassroomDto) {
  @ApiProperty({
    default: false,
  })
  is_watchable_another_student_grade: boolean;

  @ApiProperty({
    default: 'Description',
  })
  description: string;

  @ApiProperty({
    default: 'Image url',
  })
  image_url: string;

  @ApiProperty({
    default: 'class id',
  })
  @IsNotEmpty()
  @IsString()
  class_id: string;
}
