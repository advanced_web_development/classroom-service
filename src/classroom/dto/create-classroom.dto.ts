import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateClassroomDto {
  @ApiProperty({
    default: 'Advanced Web 20_4',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    default: 'Advanced Web Development',
  })
  section: string;

  @ApiProperty({
    default: 'G302',
  })
  room: string;

  @ApiProperty({
    default: 'Advanced Web Development',
  })
  subject: string;
}
