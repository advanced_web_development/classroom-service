import { ApiProperty } from '@nestjs/swagger';

export class AddTeacherByToken {
  @ApiProperty({
    default: 'abc',
  })
  token: string;
}
