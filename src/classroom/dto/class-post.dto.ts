import { ApiProperty } from '@nestjs/swagger';

export class CreateClassPost {
  @ApiProperty({
    default: 'abc',
  })
  class_id: string;

  @ApiProperty({
    default: 'content of the post',
  })
  content: string;

  @ApiProperty({
    default: 'content of the post',
  })
  title: string;
}

export class UpdateClassPost {
  @ApiProperty({
    default: 'content of the post',
  })
  content: string;
}

export class DeleteClassPost {
  @ApiProperty({
    default: 123,
  })
  id: number;
}

export class CreatePostComment {
  @ApiProperty({
    default: 2,
  })
  postId: number;

  @ApiProperty({
    default: 'content of the post',
  })
  content: string;
}
