import { Test, TestingModule } from '@nestjs/testing';
import { MailProducerController } from './mail-producer.controller';

describe('MailProducerController', () => {
  let controller: MailProducerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MailProducerController],
    }).compile();

    controller = module.get<MailProducerController>(MailProducerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
