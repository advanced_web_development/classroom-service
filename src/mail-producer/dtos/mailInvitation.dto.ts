import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class MailInvitationDto {
  @ApiProperty({
    default: 'token',
  })
  @IsNotEmpty()
  @IsString()
  token: string;

  @ApiProperty({
    default: 'https://www.youtube.com/',
  })
  @IsNotEmpty()
  @IsString()
  url: string;

  @ApiProperty({
    default: 'Role',
  })
  @IsString()
  role: string;

  @ApiProperty({
    default: 'khoanguyen517@gmail.com',
  })
  @IsString()
  email: string;
}
