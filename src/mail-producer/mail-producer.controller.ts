import { Body, Controller, Get, Post } from '@nestjs/common';
import { MailProducerService } from './mail-producer.service';
import { ApiTags } from '@nestjs/swagger';
import { MailInvitationDto } from './dtos/mailInvitation.dto';

@ApiTags('MailProducer')
@Controller('mail-producer')
export class MailProducerController {
  constructor(private mailProducerService: MailProducerService) {}

  @Post('/test')
  test(@Body() dto: MailInvitationDto) {
    console.log(dto);
    this.mailProducerService.inviteStudent({
      token: dto.token,
      role: dto.role,
      url: dto.url,
      email: dto.email,
    });
  }
}
