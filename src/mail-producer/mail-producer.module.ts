import { Module } from '@nestjs/common';
import { MailProducerService } from './mail-producer.service';
import { BullModule } from '@nestjs/bull';
import { MailProducerController } from './mail-producer.controller';

@Module({
  imports: [
    BullModule.registerQueue({
      configKey: 'mail-queue-config',
      name: 'class-invitation',
    }),
  ],
  providers: [MailProducerService],
  exports: [MailProducerService],
  controllers: [MailProducerController],
})
export class MailProducerModule {}
