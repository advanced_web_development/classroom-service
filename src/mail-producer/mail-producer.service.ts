import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';

@Injectable()
export class MailProducerService {
  constructor(
    @InjectQueue('class-invitation') private classInvitationQueue: Queue,
  ) {}

  //implement invitation task
  async inviteStudent(payload: {
    token: string;
    role: string;
    url: string;
    email: string;
  }) {
    // console.log('sending mail');
    console.log(payload);
    const job = await this.classInvitationQueue.add(
      'student_invitation',
      payload,
    );
    console.log(job.id);
  }
}
