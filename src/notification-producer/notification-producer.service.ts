import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';

@Injectable()
export class NotificationProducerService {
  constructor(@InjectQueue('notifications') private notisQueue: Queue) {}

  async createClassPost(payload) {
    const job = await this.notisQueue.add('create_class_post', payload, {
      attempts: 5,
      backoff: {
        type: 'exponential',
        delay: 1000,
      },
    });
  }

  async createPostComment(payload) {
    const job = await this.notisQueue.add(
      'create_class_post_comment',
      payload,
      {
        attempts: 5,
        backoff: {
          type: 'exponential',
          delay: 1000,
        },
      },
    );
  }
}
