import { Test, TestingModule } from '@nestjs/testing';
import { NotificationProducerService } from './notification-producer.service';

describe('NotificationProducerService', () => {
  let service: NotificationProducerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NotificationProducerService],
    }).compile();

    service = module.get<NotificationProducerService>(NotificationProducerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
