import { Module } from '@nestjs/common';
import { NotificationProducerService } from './notification-producer.service';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    BullModule.registerQueue({
      configKey: 'notifications-queue-config',
      name: 'notifications',
    }),
  ],
  providers: [NotificationProducerService],
  exports: [NotificationProducerService],
})
export class NotificationProducerModule {}
