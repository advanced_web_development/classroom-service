import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { CreateEnrollmentDto } from './dto/create-enrollment.dto';
import { UpdateEnrollmentDto } from './dto/update-enrollment.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ClassroomService } from 'src/classroom/classroom.service';
import { appConfig } from 'src/config';
import { DeleteEnrollmentDto } from './dto/delete-enrollment.dto';

@Injectable()
export class EnrollmentService {
  constructor(
    private prismaService: PrismaService,
    @Inject(forwardRef(() => ClassroomService))
    private classService: ClassroomService,
  ) {}
  async createEnrollment(
    createEnrollmentDto: CreateEnrollmentDto,
    role: string,
  ) {
    const classWithInviteCode = await this.classService.classWithInviteCode(
      createEnrollmentDto.code || '',
    );
    if (classWithInviteCode === null) {
      throw new HttpException(
        'Invite code does not exist',
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = await this.prismaService.user.findUnique({
      where: {
        id: createEnrollmentDto.user_id,
      },
    });
    if (user === null) {
      throw new HttpException('User does not exist', HttpStatus.BAD_REQUEST);
    }

    const isInClass = await this.isUserInClass(
      createEnrollmentDto.user_id,
      classWithInviteCode.id || '',
    );
    if (isInClass) {
      throw new HttpException(classWithInviteCode.id, HttpStatus.CONFLICT);
    }

    const newEnrollment = await this.prismaService.enrollment.create({
      data: {
        class_id: classWithInviteCode.id,
        user_id: createEnrollmentDto.user_id,
        role: role,
      },
      include: {
        class_entity: true,
      },
    });

    const result = await this.prismaService.enrollment.findUnique({
      where: {
        enrollment_id: {
          user_id: user.id,
          class_id: classWithInviteCode.id,
        },
      },
      include: {
        class_entity: {
          include: {
            users: {
              where: {
                role: appConfig.ROLE_CREATOR,
              },
              select: {
                user_entity: true,
              },
            },
          },
        },
      },
    });
    return result;
  }

  async createEnrollmentStudent(createEnrollmentDto: CreateEnrollmentDto) {
    return await this.createEnrollment(
      createEnrollmentDto,
      appConfig.ROLE_STUDENT,
    );
  }

  async createEnrollmentTeacher(createEnrollmentDto: CreateEnrollmentDto) {
    const isUserInClass = await this.isUserInClass(
      createEnrollmentDto.user_id,
      createEnrollmentDto.code,
    );
    if (isUserInClass) {
      const userInClass = await this.prismaService.enrollment.findUnique({
        where: {
          enrollment_id: {
            user_id: createEnrollmentDto.user_id,
            class_id: createEnrollmentDto.code,
          },
        },
      });
      if (userInClass.role === appConfig.ROLE_STUDENT) {
        //motivate user to teacher
        const updatedEnrollment = await this.prismaService.enrollment.update({
          where: {
            enrollment_id: {
              user_id: createEnrollmentDto.user_id,
              class_id: createEnrollmentDto.code,
            },
          },
          data: {
            role: appConfig.ROLE_TEACHER,
          },
        });
        return updatedEnrollment;
      }
    }
    return await this.createEnrollment(
      createEnrollmentDto,
      appConfig.ROLE_TEACHER,
    );
  }

  async findAllUserByClass(classID: string, userID: number) {
    const isInClass = await this.isUserInClass(userID, classID);
    if (!isInClass) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    const classes = await this.prismaService.enrollment.findMany({
      where: {
        class_id: classID,
      },
      include: {
        user_entity: true,
      },
    });
    return classes;
  }

  async findAllClassByUser(id: number) {
    const classes = await this.prismaService.enrollment.findMany({
      orderBy: [
        {
          created_at: 'desc',
        },
      ],
      where: {
        user_id: id,
      },
      include: {
        class_entity: {
          include: {
            users: {
              where: {
                role: appConfig.ROLE_CREATOR,
              },
              select: {
                user_entity: true,
              },
            },
          },
        },
      },
    });
    return classes;
  }

  async isUserInClass(userID: number, classID: string) {
    const foundEnrollment = await this.prismaService.enrollment.findUnique({
      where: {
        enrollment_id: {
          user_id: userID,
          class_id: classID || '',
        },
      },
    });
    if (foundEnrollment !== null) {
      return true;
    }
    return false;
  }

  async isManagerInClass(userID: number, classID: string) {
    const foundEnrollment = await this.prismaService.enrollment.findUnique({
      where: {
        enrollment_id: {
          user_id: userID,
          class_id: classID || '',
        },
      },
    });
    if (foundEnrollment !== null) {
      if (foundEnrollment.role === appConfig.ROLE_STUDENT) {
        return false;
      }
      return true;
    }
    return false;
  }

  async deleteEnrollment(
    deleteEnrollmentDto: DeleteEnrollmentDto,
    userID: number,
  ) {
    const userToDelete = await this.prismaService.enrollment.findUnique({
      where: {
        enrollment_id: {
          user_id: deleteEnrollmentDto.user_id,
          class_id: deleteEnrollmentDto.class_id,
        },
      },
    });
    if (userToDelete === null) {
      return 'User does not in class';
    }

    const userAction = await this.prismaService.enrollment.findUnique({
      where: {
        enrollment_id: {
          user_id: userID,
          class_id: deleteEnrollmentDto.class_id,
        },
      },
    });
    if (userAction === null) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    //student leave class
    if (
      userAction.role === appConfig.ROLE_STUDENT &&
      userToDelete.role === userAction.role
    ) {
      const deleteEnrollment = await this.prismaService.enrollment.delete({
        where: {
          enrollment_id: {
            user_id: deleteEnrollmentDto.user_id,
            class_id: deleteEnrollmentDto.class_id,
          },
        },
      });
      return 'Student has left the class';
    }

    const isManager = await this.isManagerInClass(
      userID,
      deleteEnrollmentDto.class_id,
    );
    if (!isManager) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    if (userToDelete.role === appConfig.ROLE_CREATOR) {
      if (userAction.role === appConfig.ROLE_CREATOR) {
        const deleteEnrollment = this.prismaService.enrollment.deleteMany({
          where: {
            class_id: deleteEnrollmentDto.class_id,
          },
        });
        const deleteClass = this.prismaService.class.delete({
          where: {
            id: deleteEnrollmentDto.class_id,
          },
        });
        const transaction = await this.prismaService.$transaction([
          deleteEnrollment,
          deleteClass,
        ]);
        return 'Class deleted';
      }
      //if user who delete creator isn't creator
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    if (userToDelete.role === appConfig.ROLE_TEACHER) {
      //only creator can delete teacher
      if (userAction.role === appConfig.ROLE_CREATOR) {
        const deleteEnrollment = await this.prismaService.enrollment.delete({
          where: {
            enrollment_id: {
              user_id: deleteEnrollmentDto.user_id,
              class_id: deleteEnrollmentDto.class_id,
            },
          },
        });
        return 'successfully';
      }
      if (
        userAction.role === appConfig.ROLE_TEACHER &&
        userToDelete.user_id === userAction.user_id
      ) {
        const deleteEnrollment = await this.prismaService.enrollment.delete({
          where: {
            enrollment_id: {
              user_id: deleteEnrollmentDto.user_id,
              class_id: deleteEnrollmentDto.class_id,
            },
          },
        });
        return 'successfully';
      }
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    if (userToDelete.role === appConfig.ROLE_STUDENT) {
      const deleteEnrollment = await this.prismaService.enrollment.delete({
        where: {
          enrollment_id: {
            user_id: deleteEnrollmentDto.user_id,
            class_id: deleteEnrollmentDto.class_id,
          },
        },
      });
      return 'successfully';
    }
    return 'successfully';
  }
}
