import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  HttpException,
  HttpStatus,
  Put,
} from '@nestjs/common';
import { EnrollmentService } from './enrollment.service';
import { CreateEnrollmentDto } from './dto/create-enrollment.dto';
import { UpdateEnrollmentDto } from './dto/update-enrollment.dto';
import { ApiHeader, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { DeleteEnrollmentDto } from './dto/delete-enrollment.dto';

@ApiTags('enrollment')
@ApiHeader({ name: 'id', description: 'id user' })
@Controller('enrollment')
export class EnrollmentController {
  constructor(private readonly enrollmentService: EnrollmentService) {}

  @Post('/student')
  async createEnrollmentStudent(
    @Body() createEnrollmentDto: CreateEnrollmentDto,
  ) {
    return await this.enrollmentService.createEnrollmentStudent(
      createEnrollmentDto,
    );
  }

  @Post('/teacher')
  async createEnrollmentTeacher(
    @Body() createEnrollmentDto: CreateEnrollmentDto,
  ) {
    return await this.enrollmentService.createEnrollmentTeacher(
      createEnrollmentDto,
    );
  }

  @Get('/class/:id')
  async findAllUserByClass(@Param('id') id: string, @Req() request: Request) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.enrollmentService.findAllUserByClass(
      id,
      +request.headers.id,
    );
  }

  @Get('/user')
  async findAllClassByUser(@Req() request: Request) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.enrollmentService.findAllClassByUser(+request.headers.id);
  }

  @Put()
  async delete(
    @Req() request: Request,
    @Body() deleteEnrollmentDto: DeleteEnrollmentDto,
  ) {
    if (request.headers.id === null) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    if (isNaN(+request.headers.id)) {
      throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
    }
    return await this.enrollmentService.deleteEnrollment(
      deleteEnrollmentDto,
      +request.headers.id,
    );
  }
}
