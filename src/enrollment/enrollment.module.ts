import { Module, forwardRef } from '@nestjs/common';
import { EnrollmentService } from './enrollment.service';
import { EnrollmentController } from './enrollment.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { ClassroomModule } from 'src/classroom/classroom.module';

@Module({
  exports: [EnrollmentService],
  controllers: [EnrollmentController],
  providers: [EnrollmentService],
  imports: [PrismaModule, forwardRef(() => ClassroomModule)],
})
export class EnrollmentModule {}
