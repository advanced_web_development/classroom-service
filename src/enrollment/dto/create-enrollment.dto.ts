import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateEnrollmentDto {
  @ApiProperty({
    default: 'uuid',
  })
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty({
    default: 1,
  })
  @IsNotEmpty()
  @IsNumber()
  user_id: number;
}
