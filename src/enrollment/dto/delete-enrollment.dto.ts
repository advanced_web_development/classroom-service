import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class DeleteEnrollmentDto {
  @ApiProperty({
    default: 'uuid',
  })
  @IsNotEmpty()
  @IsString()
  class_id: string;

  @ApiProperty({
    default: 1,
  })
  @IsNotEmpty()
  @IsNumber()
  user_id: number;
}
