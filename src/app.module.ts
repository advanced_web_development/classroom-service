import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClassroomModule } from './classroom/classroom.module';
import { EnrollmentModule } from './enrollment/enrollment.module';
import { BullModule } from '@nestjs/bull';
import { appConfig } from './config';
import { MailProducerModule } from './mail-producer/mail-producer.module';
import { PrismaModule } from './prisma/prisma.module';
import { NotificationProducerModule } from './notification-producer/notification-producer.module';

@Module({
  imports: [
    ClassroomModule,
    EnrollmentModule,
    PrismaModule,
    BullModule.forRoot('mail-queue-config', {
      redis: appConfig.MAIL_QUEUE_URL,
    }),
    BullModule.forRoot('notifications-queue-config', {
      redis: appConfig.MAIL_QUEUE_URL,
    }),
    MailProducerModule,
    NotificationProducerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
